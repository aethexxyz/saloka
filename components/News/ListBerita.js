import React, {Component } from 'react';


class ListBerita extends Component {
  render(){
    return (
      <div style={{display: 'flex', margin: '30px 0'}}>
        <div style={{width: 200}}>{this.props.tanggal}</div>
        <div style={{width: 'calc(100% - 200px)'}}>
          <h4 style={{margin: 0}}>{this.props.judul}</h4>
            <div dangerouslySetInnerHTML={{ __html: this.props.deskripsi}} />
        </div>
      </div>
    )
  }
}

export default ListBerita;
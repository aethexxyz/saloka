import React, { Component } from 'react';
import Link from 'next/link'

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: "nyo"
    }
  }

  gantiNama = () => {
    this.setState({
      nama: this.state.nama === 'nyo' ? 'nyanya' : 'nyo'
    })
  }

  render() {
    return (
      <div>
        <h1>Latihan di saloka!</h1>
        <h2>
          {this.state.nama}
        </h2>
        <ul>
          <li>
            <Link href="/about">
              <a>Ke About</a>
            </Link>
          </li>
          <li>
            <Link href="/berita">
              <a>Berita</a>
            </Link>
          </li>
        </ul>


        <br />
        <button onClick={() => this.gantiNama()}>
          PENCETAN
        </button>
      </div>
    )
  }
}

export default Index
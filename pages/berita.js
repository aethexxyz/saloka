import React, { Component } from 'react';
import ListBerita from '../components/News/ListBerita';
import axios from 'axios';
import Link from 'next/link'
class Berita extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true
    }
  }

  componentDidMount() {
    axios.get('http://localhost/pht/wp-json/wp/v2/posts')
      .then(res => {
        this.setState({
          data: res.data,
          loading: false
        })
      })
      .catch(err => err)
  }

  viewLoading = () => (
    <div style={{ display: 'flex', marginBottom: 20 }}>
      <div style={{ width: 180, height: 14, background: '#eee' }} />
      <div style={{ marginLeft: 20 }}>
        <div style={{ width: 400, height: 16, background: '#eee', marginBottom: 20 }} />
        <div style={{ width: 500, height: 14, background: '#eee', marginBottom: 10 }} />
        <div style={{ width: 500, height: 14, background: '#eee', marginBottom: 10 }} />
        <div style={{ width: 500, height: 14, background: '#eee', marginBottom: 10 }} />
        <div style={{ width: 500, height: 14, background: '#eee', marginBottom: 10 }} />
      </div>
    </div>
  )
  viewMain = () => {
    return (
      <React.Fragment>
        {
          this.state.data.map(i => (
            <ListBerita
              key={i.id}
              tanggal={i.date}
              judul={i.title.rendered}
              deskripsi={i.content.rendered}
            />

          ))}
      </React.Fragment>
    )
  }
  render() {
    return (
      <div>
        <h1>Halaman Berita</h1>
        <ul>
          <li>
            <Link href="/">
              <a>Ke Index</a>
            </Link>
          </li>
          <li>
            <Link href="/about">
              <a>Ke About</a>
            </Link>
          </li>
          <li>
            <Link href="/berita">
              <a>Berita</a>
            </Link>
          </li>
        </ul>

        {
          this.state.loading ? 
            (
              <div>
                {this.viewLoading() }
                {this.viewLoading() }
                {this.viewLoading() }
              </div>
            )
          
          : this.viewMain()
        }


      </div>
    )
  }
}

export default Berita;